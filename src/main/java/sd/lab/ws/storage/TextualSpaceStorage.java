package sd.lab.ws.storage;

import sd.lab.linda.textual.TextualSpace;

import java.util.Optional;
import java.util.stream.Stream;

public interface TextualSpaceStorage extends Storage {

    TextualSpace create(String name);

    Optional<TextualSpace> get(String name);

    TextualSpace createIfAbsentOrGet(String name);

    boolean contains(String name);

    Stream<TextualSpace> getAll();

    static TextualSpaceStorage getInstance() {
        return new TextualSpaceStorageImpl();
    }
}
