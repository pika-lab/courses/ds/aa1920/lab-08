package sd.lab.ws.storage;

import sd.lab.linda.textual.TextualSpace;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

public class TextualSpaceStorageImpl implements TextualSpaceStorage {
    private final Map<String, TextualSpace> tupleSpaces = new HashMap<>();

    @Override
    public Optional<TextualSpace> get(String name) {
        return Optional.ofNullable(tupleSpaces.get(name));
    }

    @Override
    public TextualSpace createIfAbsentOrGet(String name) {
        return tupleSpaces.computeIfAbsent(name, TextualSpace::of);
    }

    @Override
    public TextualSpace create(String name) {
        TextualSpace ts = TextualSpace.of(name);
        tupleSpaces.put(name, ts);
        return ts;
    }

    @Override
    public boolean contains(String name) {
        return tupleSpaces.containsKey(name);
    }

    @Override
    public Stream<TextualSpace> getAll() {
        return tupleSpaces.values().stream();
    }
}
