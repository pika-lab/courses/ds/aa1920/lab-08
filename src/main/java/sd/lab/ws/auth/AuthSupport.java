package sd.lab.ws.auth;

import io.vertx.core.http.HttpHeaders;
import io.vertx.ext.web.RoutingContext;
import sd.lab.ws.exceptions.ForbiddenError;
import sd.lab.ws.exceptions.UnauthorizedError;
import sd.lab.ws.presentation.UserData;
import sd.lab.ws.storage.SecretStorage;
import sd.lab.ws.storage.UserStorage;

import java.io.IOException;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public interface AuthSupport {

    public static final Pattern JWT_PATTERN = Pattern.compile("^(?:(?:[jJ][wW][tT])?\\s+)?([^\\s]*)\\s*$");

    UserStorage getUserStorage();

    SecretStorage getSecretStorage();

    default String getAuthorizationHeader(RoutingContext routingContext) {
        return routingContext.request().getHeader(HttpHeaders.AUTHORIZATION);
    }

    default Optional<UserData> getAuthenticatedUser(RoutingContext routingContext) {
        Optional<UserData> credentials = Optional.empty();

        try {
            if (getAuthorizationHeader(routingContext) != null) {
                final Matcher m = JWT_PATTERN.matcher(getAuthorizationHeader(routingContext));
                if (m.matches()) {
                    final String jwtString = m.group(1); // base64 encoding of the JWT
                    final JsonWebToken jwt = null;
                    // TODO parse JWT from jwtString

                    if (jwt.verify(getSecretStorage())) {
                        credentials = Optional.ofNullable(jwt)
                                .map(JsonWebToken::getPayload)
                                .map(JsonWebToken.Payload::getUser)
                                .map(u -> {
                                    final Optional<UserData> actualUser = getUserStorage().find(u);
                                    if (actualUser.isPresent()) {
                                        // TODO handle this case
                                        throw new IllegalStateException("not implemented");
                                    } else {
                                        // TODO handle this case
                                        throw new IllegalStateException("not implemented");
                                    }
                                });
                    } else {
                        // TODO handle this case
                        throw new IllegalStateException("not implemented");
                    }
                } else {
                    // TODO handle this case
                    throw new IllegalStateException("not implemented");
                }
            }
        } catch (Exception e) {
            // TODO handle this case
            throw new IllegalStateException("not implemented");
        }

        return credentials;
    }

    default boolean isAuthenticatedUserAtLeast(RoutingContext routingContext, UserData.Role role) {
        return getAuthenticatedUser(routingContext).isPresent()
                && getAuthenticatedUser(routingContext).get().getRole().compareTo(role) >= 0;
    }

    default void ensureAuthenticatedUserAtLeast(RoutingContext routingContext, UserData.Role role) {
        if (!getAuthenticatedUser(routingContext).isPresent()) {
            throw new UnauthorizedError();
        }
        if (getAuthenticatedUser(routingContext).get().getRole().compareTo(role) < 0) {
            throw new ForbiddenError();
        }
    }
}
