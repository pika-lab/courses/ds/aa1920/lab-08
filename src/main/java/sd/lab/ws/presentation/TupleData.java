package sd.lab.ws.presentation;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import sd.lab.linda.textual.StringTuple;

import java.io.IOException;
import java.util.Objects;

@JacksonXmlRootElement(localName = "tuple")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TupleData extends Data {

    private String value = null;

    public TupleData() {

    }

    public TupleData(String value) {
        this.value = value;
    }

    public TupleData(StringTuple tuple) {
        this.value = tuple.getValue();
    }

    public TupleData(TupleData clone) {
        this(clone.value);
    }

    @JsonProperty("value")
    @JacksonXmlProperty(localName = "value")
    public String getValue() {
        return value;
    }

    public TupleData setValue(String value) {
        this.value = value;
        return this;
    }

    public StringTuple toTuple() {
        return StringTuple.of(getValue());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TupleData link = (TupleData) o;
        return Objects.equals(value, link.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return "TupleRepresentation{" +
                "value='" + value + '\'' +
                '}';
    }

    public static TupleData fromJSON(String representation) throws IOException {
        return Data.fromJSON(representation, TupleData.class);
    }

    public static TupleData fromYAML(String representation) throws IOException {
        return Data.fromYAML(representation, TupleData.class);
    }

    public static TupleData fromXML(String representation) throws IOException {
        return Data.fromXML(representation, TupleData.class);
    }

    public static TupleData parse(String mimeType, String payload) throws IOException {
        return parse(mimeType, payload, TupleData.class);
    }

}
