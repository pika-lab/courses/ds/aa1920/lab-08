package sd.lab.ws.presentation;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

@JacksonXmlRootElement(localName = "listOfLinks")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ListOfLinkData extends ListData<LinkData> {

    public ListOfLinkData() {
    }

    public ListOfLinkData(Collection<? extends LinkData> collection) {
        super(collection);
    }

    public ListOfLinkData(Stream<? extends LinkData> stream) {
        super(stream);
    }

    public ListOfLinkData(LinkData element1, LinkData... elements) {
        super(element1, elements);
    }

    @JsonProperty("links")
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "link")
    public List<LinkData> getLinks() {
        return getItems();
    }

    public ListOfLinkData setLinks(List<LinkData> users) {
        setItems(users);
        return this;
    }

    public static ListOfLinkData fromJSON(String representation) throws IOException {
        return Data.fromJSON(representation, ListOfLinkData.class);
    }

    public static ListOfLinkData fromYAML(String representation) throws IOException {
        return Data.fromYAML(representation, ListOfLinkData.class);
    }

    public static ListOfLinkData fromXML(String representation) throws IOException {
        return Data.fromXML(representation, ListOfLinkData.class);
    }

    public static ListOfLinkData parse(String mimeType, String payload) throws IOException {
        return parse(mimeType, payload, ListOfLinkData.class);
    }
}
