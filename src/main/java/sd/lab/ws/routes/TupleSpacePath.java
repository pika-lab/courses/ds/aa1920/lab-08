package sd.lab.ws.routes;

import io.vertx.core.Promise;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.ParsedHeaderValue;
import io.vertx.ext.web.ParsedHeaderValues;
import io.vertx.ext.web.RoutingContext;
import sd.lab.linda.textual.RegexTemplate;
import sd.lab.ws.api.TupleSpaceApi;
import sd.lab.ws.exceptions.BadContentError;
import sd.lab.ws.exceptions.HttpError;
import sd.lab.ws.exceptions.InternalServerError;
import sd.lab.ws.presentation.*;
import sd.lab.ws.storage.SecretStorage;
import sd.lab.ws.storage.TextualSpaceStorage;
import sd.lab.ws.storage.UserStorage;

import java.io.IOException;
import java.util.Optional;

import static sd.lab.ws.presentation.MIMETypes.*;

public class TupleSpacePath extends Path {

    private final TextualSpaceStorage tupleSpaceStorage;

    public TupleSpacePath(UserStorage userStorage, SecretStorage secretStorage, TextualSpaceStorage tsStorage) {
        super("/tuple-spaces", userStorage, secretStorage);
        tupleSpaceStorage = tsStorage;
    }

    @Override
	protected void setupRoutes() {
        addRoute(HttpMethod.GET, this::getTupleSpaces)
                .produces(APPLICATION_JSON)
                .produces(APPLICATION_XML)
                .produces(APPLICATION_YAML);

        addRoute(HttpMethod.GET, "/:tupleSpaceName", this::getTuple)
                .produces(APPLICATION_JSON)
                .produces(APPLICATION_XML)
                .produces(APPLICATION_YAML);

        addRoute(HttpMethod.DELETE, "/:tupleSpaceName", this::deleteTuple)
                .produces(APPLICATION_JSON)
                .produces(APPLICATION_XML)
                .produces(APPLICATION_YAML);

        addRoute(HttpMethod.POST, "/:tupleSpaceName", this::postTuple)
                .consumes(APPLICATION_JSON)
                .consumes(APPLICATION_XML)
                .consumes(APPLICATION_YAML)
                .produces(APPLICATION_JSON)
                .produces(APPLICATION_XML)
                .produces(APPLICATION_YAML);
	}

    private void getTupleSpaces(RoutingContext routingContext) {
        final TupleSpaceApi api = TupleSpaceApi.get(routingContext, getUserStorage(), getSecretStorage(), getTupleSpaceStorage());
        final Promise<ListOfLinkData> result = Promise.promise();
        result.future().setHandler(responseHandler(routingContext, this::cleanGetTupleSpacesInputsResult));

        try {
            final Optional<Integer> skip = Optional.ofNullable(routingContext.queryParams().get("skip")).map(Integer::parseInt);
            final Optional<Integer> limit = Optional.ofNullable(routingContext.queryParams().get("limit")).map(Integer::parseInt);
            final Optional<String> filter = Optional.ofNullable(routingContext.queryParams().get("filter"));
            validateGetTupleSpacesInputs(skip, limit, filter);

            api.getAllTupleSpaces(skip.orElse(0), limit.orElse(10), filter.orElse(""), result);
        } catch(HttpError e) {
            result.fail(e);
        } catch (IllegalArgumentException e) {
            result.fail(new BadContentError(e));
        }
	}

    private ListOfLinkData cleanGetTupleSpacesInputsResult(ListOfLinkData links) {
        requireNoneIsNull(links);
        return cleanLinks(links);
    }

    private void validateGetTupleSpacesInputs(Optional<Integer> skip, Optional<Integer> limit, Optional<String> filter) {
        // always ok
    }

    private ListOfLinkData cleanLinks(ListOfLinkData links) {
        return new ListOfLinkData(
                links.stream().map(this::cleanLink)
        );
    }

    private LinkData cleanLink(LinkData link) {
        return new LinkData(getPath() + link.getUrl());
    }

    private void deleteTuple(RoutingContext routingContext) {
        final TupleSpaceApi api = TupleSpaceApi.get(routingContext, getUserStorage(), getSecretStorage(), getTupleSpaceStorage());
        final Promise<TupleData> result = Promise.promise();
        result.future().setHandler(responseHandler(routingContext, this::cleanDeleteTupleResult));

        try {
            final String tupleSpaceName = routingContext.pathParam("tupleSpaceName");
            final Optional<String> template = Optional.ofNullable(routingContext.queryParams().get("template"));
            validateDeleteTupleInputs(tupleSpaceName, template);

            api.consumeTuple(tupleSpaceName, RegexTemplate.of(template.get()), result);
        } catch(HttpError e) {
            result.fail(e);
        } catch (IllegalArgumentException e) {
            result.fail(new BadContentError(e));
        }
    }

    private void validateDeleteTupleInputs(String tupleSpaceName, Optional<String> template) {
        requireParameterIsPresent(template);
    }

    private TupleData cleanDeleteTupleResult(TupleData tuple) {
        requireNoneIsNull(tuple);
        return tuple;
    }

    private void getTuple(RoutingContext routingContext) {
        final TupleSpaceApi api = TupleSpaceApi.get(routingContext, getUserStorage(), getSecretStorage(), getTupleSpaceStorage());
        final Promise<Data> result = Promise.promise();
        result.future().setHandler(responseHandler(routingContext, this::cleanGetTupleResult));

        try {
            final String tupleSpaceName = routingContext.pathParam("tupleSpaceName");
            final Optional<Boolean> count = Optional.ofNullable(routingContext.queryParams().get("count")).map(Boolean::parseBoolean);
            final Optional<String> template = Optional.ofNullable(routingContext.queryParams().get("template"));
            validateGetTupleInputs(tupleSpaceName, count, template);

            if (count.isPresent() && count.orElse(false)) {
                api.countAllTuples(tupleSpaceName, result);
            } else if(template.isPresent()) {
                api.observeTuple(tupleSpaceName, RegexTemplate.of(template.get()), result);
            } else {
                api.getAllTuples(tupleSpaceName, result);
            }
        } catch(HttpError e) {
            result.fail(e);
        } catch (IllegalArgumentException e) {
            result.fail(new BadContentError(e));
        }
    }

    private void validateGetTupleInputs(String tupleSpaceName, Optional<Boolean> count, Optional<String> template) {
        // always ok
    }

    private Data cleanGetTupleResult(Data x) {
        if (x instanceof NumberData) {
            NumberData n = (NumberData) x;
            if (n.getValue().longValue() >= 0) {
                return n;
            }
        } else if (x instanceof ListOfTupleData) {
            return x;
        } else if (x instanceof TupleData) {
            return x;
        }

        throw new InternalServerError();
    }

    private void postTuple(RoutingContext routingContext) {
        final TupleSpaceApi api = TupleSpaceApi.get(routingContext, getUserStorage(), getSecretStorage(), getTupleSpaceStorage());
        final Promise<TupleData> result = Promise.promise();
        result.future().setHandler(responseHandler(routingContext, this::cleanPostTupleResult));

        try {
            final String tupleSpaceName = routingContext.pathParam("tupleSpaceName");
            final Optional<String> tuple =  Optional.ofNullable(routingContext.getBodyAsString());
            final Optional<String> contentType = Optional.ofNullable(routingContext.parsedHeaders())
                    .map(ParsedHeaderValues::contentType)
                    .map(ParsedHeaderValue::value);
            validatePostTupleInputs(tupleSpaceName, tuple, contentType);

            TupleData tupleData = TupleData.parse(contentType.get(), tuple.get());

            api.insertTuple(tupleSpaceName, tupleData.toTuple(), result);
        } catch(HttpError e) {
            result.fail(e);
        } catch (IllegalArgumentException | IOException e) {
            result.fail(new BadContentError(e));
        }
    }

    private TupleData cleanPostTupleResult(TupleData x) {
        return cleanTuple(x);
    }

    private void validatePostTupleInputs(String tupleSpaceName, Optional<String> tuple, Optional<String> contentType) {
        requireStringIsNotBlank(tuple);
        requireStringIsNotBlank(contentType);
        requireOneOf(contentType.get(), APPLICATION_JSON, APPLICATION_XML, APPLICATION_YAML);
    }

    private TupleData cleanTuple(TupleData tuple) {
        requireNoneIsNull(tuple);
        return tuple;
    }

    public TextualSpaceStorage getTupleSpaceStorage() {
        return tupleSpaceStorage;
    }
}
