package sd.lab.ws.routes;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import sd.lab.ws.api.AuthApi;
import sd.lab.ws.auth.JsonWebToken;
import sd.lab.ws.exceptions.BadContentError;
import sd.lab.ws.exceptions.HttpError;
import sd.lab.ws.exceptions.InternalServerError;
import sd.lab.ws.presentation.UserData;
import sd.lab.ws.storage.SecretStorage;
import sd.lab.ws.storage.UserStorage;

import java.io.IOException;
import java.util.Optional;
import java.util.function.Function;

import static sd.lab.ws.presentation.MIMETypes.*;

public class AuthPath extends Path {

    public AuthPath(UserStorage userStorage, SecretStorage secretStorage) {
        super("/auth", userStorage, secretStorage);
    }

    @Override
    protected void setupRoutes() {
        // TODO notice this
        addRoute(HttpMethod.POST, "/token", this::post)
                .consumes(APPLICATION_JSON)
                .consumes(APPLICATION_XML)
                .consumes(APPLICATION_YAML)
                .produces(APPLICATION_JWT);
    }

    private void post(RoutingContext routingContext) {
        final AuthApi api = AuthApi.get(routingContext, getUserStorage(), getSecretStorage());
        final Promise<JsonWebToken> result = Promise.promise();
        result.future().setHandler(jwtResponseHandler(routingContext, this::cleanUpToken));

        try {
            final UserData user = UserData.parse(routingContext.parsedHeaders().contentType().value(), routingContext.getBodyAsString());
            final Optional<Integer> duration = Optional.ofNullable(routingContext.queryParams().get("duration")).map(Integer::parseInt);

            validateCredentialsForPost(user);

            // TODO notice this
            api.createToken(user, duration.orElse(null), result);
        } catch(HttpError e) {
            result.fail(e);
        } catch (IOException | IllegalArgumentException e) {
            result.fail(new BadContentError(e));
        }
    }

    private JsonWebToken cleanUpToken(JsonWebToken token) {
        if (token.getPayload() != null && token.getPayload().getUser() != null) {
            final UserData u = new UserData(token.getPayload().getUser());
            // TODO cleanup the user's password in x-user clain
            // TODO ensure the user's url in x-user claim is correct
            // TODO ensure the JWT `sub` field is an URL referring to the same user described by the x-user claim
        }
        // TODO sign the JWT
        if (!token.verify(getSecretStorage())) {
            throw new InternalServerError();
        }
        return token;
    }

    private void validateCredentialsForPost(UserData user) {
        requireNoneIsNull(user.getPassword());
        requireSomeIsNonNull(user.getId(), user.getEmail(), user.getUsername());
    }

    protected  <X extends JsonWebToken> Handler<AsyncResult<X>> jwtResponseHandler(RoutingContext routingContext, Function<X, X> cleaner) {
        return x -> {
            if (x.failed() && x.cause() instanceof HttpError) {
                final HttpError exception = (HttpError) x.cause();
                routingContext.response()
                        .setStatusCode(exception.getStatusCode())
                        .end(exception.getMessage());
            } else if (x.failed()) {
                routingContext.response()
                        .setStatusCode(500)
                        .end("Internal Server Error");
            } else {
                try {
                    final X cleanResult = cleaner.apply(x.result());
                    final String result = cleanResult.toBase64Signed(getSecretStorage());
                    routingContext.response()
                            .putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JWT)
                            .setStatusCode(200)
                            .end(result);
                } catch(HttpError e) {
                    routingContext.response()
                            .setStatusCode(e.getStatusCode())
                            .end(e.getMessage());
                } catch (Exception e) {
                    routingContext.response()
                            .setStatusCode(500)
                            .end("Internal Server Error");
                }
            }
        };
    }
}
