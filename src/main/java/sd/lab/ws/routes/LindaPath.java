package sd.lab.ws.routes;

import sd.lab.ws.storage.SecretStorage;
import sd.lab.ws.storage.TextualSpaceStorage;
import sd.lab.ws.storage.UserStorage;

public class LindaPath extends Path {

    private final TextualSpaceStorage textualSpaceStorage = TextualSpaceStorage.getInstance();

	public LindaPath(String version) {
		super(
		        "/linda/v" + version,
                UserStorage.getInstance(),
                SecretStorage.getInstance()
            );
	}

    @Override
    protected void setupRoutes() {
        append(new UsersPath(getUserStorage(), getSecretStorage()));
        append(new AuthPath(getUserStorage(), getSecretStorage()));
        append(new TupleSpacePath(getUserStorage(), getSecretStorage(), getTextualSpaceStorage()));
    }

    public TextualSpaceStorage getTextualSpaceStorage() {
        return textualSpaceStorage;
    }
}
