package sd.lab.ws.api;

import io.vertx.core.Promise;
import io.vertx.ext.web.RoutingContext;
import sd.lab.ws.auth.JsonWebToken;
import sd.lab.ws.exceptions.NotFoundError;
import sd.lab.ws.exceptions.UnauthorizedError;
import sd.lab.ws.presentation.UserData;
import sd.lab.ws.storage.SecretStorage;
import sd.lab.ws.storage.UserStorage;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.Optional;
import java.util.UUID;

public class AuthApiImpl extends AbstractApi implements AuthApi {

    public AuthApiImpl(RoutingContext routingContext, UserStorage userStorage, SecretStorage secretStorage) {
        super(routingContext, userStorage, secretStorage);
    }

    @Override
    public void createToken(UserData credentials, Integer duration, Promise<? super JsonWebToken> promise) {
        final Optional<UserData> optUser = getUserStorage().find(credentials);
        if (optUser.isPresent()) {
            final UserData user = optUser.get();
            // TODO handle this case
            // TODO you can rely on User::checkPassword method
            // TODO you can rely on generateToken method below
            throw new IllegalStateException("not implemented");
        } else {
            promise.fail(new UnauthorizedError());
        }
    }

    private JsonWebToken generateToken(UserData user, Integer duration) {
        final OffsetDateTime now = OffsetDateTime.now();

        final JsonWebToken jwt = new JsonWebToken();
        jwt.getHeader()
                .setAlg(JsonWebToken.Header.ALG_SIMPLE_HMAC)
                .setTyp(JsonWebToken.Header.TYP_JWT);
        // TODO set up the JWT `jti` field
        // TODO set up the JWT `iat` field
        // TODO set up the JWT `sub` field
        // TODO set up the JWT `x-user` field
        throw new IllegalStateException("not implemented");
        /* TODO uncomment this after removing the exception above
        if (duration != null) {
            // TODO set up the JWT `exp` field
            throw new IllegalStateException("not implemented");
        }

        return jwt;
         */
    }
}
