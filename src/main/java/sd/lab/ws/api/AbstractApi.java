package sd.lab.ws.api;

import io.vertx.ext.web.RoutingContext;
import sd.lab.ws.storage.SecretStorage;
import sd.lab.ws.storage.UserStorage;

import java.util.Objects;

abstract class AbstractApi implements Api {
    private final RoutingContext routingContext;
    private final UserStorage userStorage;
    private final SecretStorage secretStorage;

    AbstractApi(RoutingContext routingContext, UserStorage userStorage, SecretStorage secretStorage) {
        this.routingContext = Objects.requireNonNull(routingContext);
        this.userStorage = Objects.requireNonNull(userStorage);
        this.secretStorage = Objects.requireNonNull(secretStorage);
    }

    @Override
    public RoutingContext getRoutingContext() {
        return routingContext;
    }

    public UserStorage getUserStorage() {
        return userStorage;
    }

    @Override
    public SecretStorage getSecretStorage() {
        return secretStorage;
    }
}
