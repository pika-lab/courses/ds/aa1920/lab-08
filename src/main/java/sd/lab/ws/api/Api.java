package sd.lab.ws.api;

import io.vertx.ext.web.RoutingContext;
import sd.lab.ws.auth.AuthSupport;
import sd.lab.ws.presentation.UserData;

import java.util.Objects;
import java.util.Optional;

public interface Api extends AuthSupport {

    RoutingContext getRoutingContext();

    default String getPath() {
        final String path = getRoutingContext().request().path();
        final int queryIndex = path.indexOf('?');
        return path.substring(0, queryIndex > 0 ? queryIndex : path.length());
    }

    default String getPath(String subPath) {
        return getPath() + "/" + Objects.requireNonNull(subPath);
    }

    default String getAuthorizationHeader() {
        return getAuthorizationHeader(getRoutingContext());
    }

    default Optional<UserData> getAuthenticatedUser() {
        return getAuthenticatedUser(getRoutingContext());
    }

    default boolean isAuthenticatedUserAtLeast(UserData.Role role) {
        return isAuthenticatedUserAtLeast(getRoutingContext(), role);
    }

    default void ensureAuthenticatedUserAtLeast(UserData.Role role) {
        ensureAuthenticatedUserAtLeast(getRoutingContext(), role);
    }
}
