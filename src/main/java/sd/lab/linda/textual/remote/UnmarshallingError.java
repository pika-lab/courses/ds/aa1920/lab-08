package sd.lab.linda.textual.remote;

import sd.lab.ws.presentation.Data;

public class UnmarshallingError extends IllegalStateException {

    private final String raw;
    private final String mimeType;
    private final Class<? extends Data> clazz;

    public UnmarshallingError(String raw, String mimeType, Class<? extends Data> clazz) {
        this.raw = raw;
        this.mimeType = mimeType;
        this.clazz = clazz;
    }

    public UnmarshallingError(String s, String raw, String mimeType, Class<? extends Data> clazz) {
        super(s);
        this.raw = raw;
        this.mimeType = mimeType;
        this.clazz = clazz;
    }

    public UnmarshallingError(String message, Throwable cause, String raw, String mimeType, Class<? extends Data> clazz) {
        super(message, cause);
        this.raw = raw;
        this.mimeType = mimeType;
        this.clazz = clazz;
    }

    public UnmarshallingError(Throwable cause, String raw, String mimeType, Class<? extends Data> clazz) {
        super(cause);
        this.raw = raw;
        this.mimeType = mimeType;
        this.clazz = clazz;
    }

    public String getRaw() {
        return raw;
    }

    public String getMimeType() {
        return mimeType;
    }

    public Class<? extends Data> getClazz() {
        return clazz;
    }

}
