package sd.lab.linda.textual.remote;

import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.commons.collections4.KeyValue;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.keyvalue.DefaultKeyValue;
import org.apache.commons.collections4.multiset.HashMultiSet;
import sd.lab.linda.textual.RegexTemplate;
import sd.lab.linda.textual.StringTuple;
import sd.lab.linda.textual.TextualSpace;
import sd.lab.ws.presentation.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.time.Duration;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import static io.vertx.core.http.HttpMethod.POST;
import static sd.lab.ws.presentation.MIMETypes.APPLICATION_JWT;

public class RemoteTextualSpaceImpl implements TextualSpace {
    private static final Logger LOGGER = LoggerFactory.getLogger(RemoteTextualSpaceImpl.class);
    private static final Duration MAX_WAIT = Duration.ofSeconds(2);
    private static final String BASE_URL = "/linda/v2";
    private static final String NO_HEADER = null;
    private static final String NO_TOKEN = null;

    private final HttpClient client;
    private final String host;
    private final int port;
    private final String name;
    private final String mimeType;
    private final UserData credentials;
    private String token;


    public RemoteTextualSpaceImpl(HttpClient client, String host, int port, String name, String mimeType, String username, String password) throws InterruptedException, ExecutionException, TimeoutException {
        this.client = Objects.requireNonNull(client);
        this.host = Objects.requireNonNull(host);
        this.port = port;
        this.name = Objects.requireNonNull(name);
        this.mimeType = Objects.requireNonNull(mimeType);
        this.credentials = new UserData()
                .setUsername(Objects.requireNonNull(username))
                .setPassword(Objects.requireNonNull(password));

        // TODO retrieve a token from `credentials`
        // TODO consider using the getTokenForUser method below
    }

    private static KeyValue<String, Object> query(String key, Object value) {
        return new DefaultKeyValue<>(key, value);
    }

    private static String urlEncode(String x) {
        try {
            return URLEncoder.encode(x, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e);
        }
    }

    private CompletableFuture<String> getTokenForUser(UserData credentials) {
        return getTokenForUser(credentials, Optional.empty());
    }

    private CompletableFuture<String> getTokenForUser(UserData credentials, Optional<Integer> duration) {
        final CompletableFuture<String> token = new CompletableFuture<>();
        request(POST, "/auth/token", NO_TOKEN, mimeType, APPLICATION_JWT, duration.map(d -> query("duration", d)).orElse(null))
                .handler(res -> {
                    if (res.statusCode() != 200) {
                        token.completeExceptionally(new HttpStatusError(res.statusCode()));
                    } else {
                        res.bodyHandler(body -> {
                            if (body != null && body.length() > 0) {
                                token.complete(body.toString());
                            } else {
                                token.completeExceptionally(new HttpStatusError(res.statusCode()));
                            }
                        });
                    }
                }).end(credentials.toMIMETypeString(mimeType));
        return token;
    }

    private HttpClientRequest request(HttpMethod method, String token, String contentType, String accept, KeyValue<String, Object>... queries) {
        return request(method, "/tuple-spaces/" + name, token, contentType, accept, queries);
    }

    private HttpClientRequest request(HttpMethod method, String contentType, String accept, KeyValue<String, Object>... queries) {
        return request(method, token, contentType, accept, queries);
    }

    private HttpClientRequest request(HttpMethod method, String path, String token, String contentType, String accept, KeyValue<String, Object>... queries) {
        final String queryString = Arrays.stream(queries)
                .filter(Objects::nonNull)
                .map(kv -> kv.getKey() + "=" + urlEncode(kv.getValue().toString()))
                .collect(Collectors.joining("&", "?", ""));
        HttpClientRequest temp = client.request(method, port, host, BASE_URL + path + queryString);
        temp = temp.connectionHandler(conn -> LOGGER.debug("Request {0} {1}", method, "http://" + host + ":" + port + BASE_URL + path + queryString));
        if (token != null) {
            temp = temp.putHeader("Authorization", token);
        }
        if (contentType != null) {
            temp = temp.putHeader("Content-Type", contentType);
        }
        if (accept != null) {
            temp = temp.putHeader("Accept", accept);
        }
        return temp;
    }

    @Override
    public CompletableFuture<StringTuple> rd(RegexTemplate template) {
        final CompletableFuture<StringTuple> result = new CompletableFuture<>();
        // TODO implement rd, consider using the request method above
        result.completeExceptionally(new IllegalStateException("not implemented"));
        return result;
    }

    @Override
    public CompletableFuture<StringTuple> in(RegexTemplate template) {
        final CompletableFuture<StringTuple> result = new CompletableFuture<>();
        // TODO implement in, consider using the request method above
        result.completeExceptionally(new IllegalStateException("not implemented"));
        return result;
    }

    @Override
    public CompletableFuture<StringTuple> out(StringTuple tuple) {
        final CompletableFuture<StringTuple> result = new CompletableFuture<>();
        // TODO implement out, consider using the request method above
        result.completeExceptionally(new IllegalStateException("not implemented"));
        return result;
    }

    @Override
    public CompletableFuture<MultiSet<? extends StringTuple>> get() {
        final CompletableFuture<MultiSet<? extends StringTuple>> result = new CompletableFuture<>();
        request(HttpMethod.GET, NO_HEADER, mimeType)
                .handler(res -> {
                    if (res.statusCode() == 200) {
                        res.bodyHandler(body -> {
                            try {
                                ListOfTupleData tuplesData = ListOfTupleData.parse(mimeType, body.toString());
                                result.complete(new HashMultiSet<>(tuplesData.toListOfTuples()));
                            } catch (IOException e) {
                                result.completeExceptionally(new UnmarshallingError(body.toString(), mimeType, ListOfTupleData.class));
                            }
                        });
                    } else if (res.statusCode() == 204) {
                        result.complete(new HashMultiSet<>());
                    } else {
                        result.completeExceptionally(new HttpStatusError(res.statusCode()));
                    }
                })
                .end();
        return result;
    }

    @Override
    public CompletableFuture<Integer> getSize() {
        final CompletableFuture<Integer> result = new CompletableFuture<>();
        request(HttpMethod.GET, NO_HEADER, mimeType, query("count", true))
                .handler(res -> {
                    if (res.statusCode() == 200) {
                        res.bodyHandler(body -> {
                            try {
                                NumberData tuplesData = NumberData.parse(mimeType, body.toString());
                                result.complete(tuplesData.getValue().intValue());
                            } catch (IOException e) {
                                result.completeExceptionally(new UnmarshallingError(body.toString(), mimeType, NumberData.class));
                            }
                        });
                    } else {
                        result.completeExceptionally(new HttpStatusError(res.statusCode()));
                    }
                })
                .end();
        return result;
    }

    @Override
    public String getName() {
        return name;
    }
}
