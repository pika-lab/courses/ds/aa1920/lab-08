package sd.lab.linda.textual.remote;

public class HttpStatusError extends IllegalStateException {

    private final int statusCode;
    public HttpStatusError(int statusCode) {
        this.statusCode = statusCode;
    }

    public HttpStatusError(int statusCode, String message) {
        super(message);
        this.statusCode = statusCode;
    }

    public HttpStatusError(int statusCode, String message, Throwable cause) {
        super(message, cause);
        this.statusCode = statusCode;
    }

    public HttpStatusError(int statusCode, Throwable cause) {
        super(cause);
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

}
