package sd.lab.linda.textual;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import sd.lab.ThreadAgent;
import sd.lab.test.ConcurrentTestHelper;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TestNegatedTemplate {
    
    private static ExecutorService executor;

    @BeforeClass
    public static void setUpUnit() throws Exception {
        executor = Executors.newSingleThreadExecutor();
    }
    
    private TextualSpace tupleSpace;
    private ConcurrentTestHelper test;
    
    @Before
    public void setUp() throws Exception {
        tupleSpace = TextualSpace.of(executor);
        test = new ConcurrentTestHelper();
    }
    
    @Test
    public void negateTemplateExample() {
        test.setThreadCount(2);

        ThreadAgent alice = new ThreadAgent("Alice") {
            @Override
            protected void loop() throws Exception {
                test.assertEquals(
                        tupleSpace.in("^(?![cC]).*?$"), // WTF? https://regex101.com/r/66NTyD/1
                        StringTuple.of("cab")
                );
                stop();
            }

            @Override
            protected void onEnd() {
                test.done();
            }
        }.start();

        ThreadAgent bob = new ThreadAgent("Bob") {
            @Override
            protected void loop() throws Exception {
                tupleSpace.out("abc").get();
                tupleSpace.out("bac").get();
                tupleSpace.out("cab").get();
                stop();
            }

            @Override
            protected void onEnd() {
                test.done();
            }
        }.start();
    }
    
}
