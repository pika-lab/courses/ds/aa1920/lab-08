package sd.lab.ws;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sd.lab.linda.textual.AbstractTestTextualSpace;
import sd.lab.linda.textual.TextualSpace;
import sd.lab.linda.textual.remote.HttpStatusError;
import sd.lab.linda.textual.remote.RemoteTextualSpaceImpl;
import sd.lab.test.ConcurrentTestHelper;
import sd.lab.ws.presentation.MIMETypes;

import java.time.Duration;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class TestRemoteTextualSpaceAccessControl {

    private static final Duration MAX_WAIT = Duration.ofSeconds(5);
    private TextualSpace tupleSpace;

    @BeforeClass
    public static void setUpUnit() throws Exception {
        AbstractTestTextualSpace.setUpUnit();

        service = Service.start(Vertx.vertx()).awaitDeployment(MAX_WAIT);

        client = Vertx.vertx().createHttpClient(
                new HttpClientOptions()
                        .setMaxPoolSize(1 << 15)
                        .setHttp2MaxPoolSize(1 << 15)
        );
    }

    @Parameterized.Parameters
    public static Iterable<String> data() {
        return Stream.of(MIMETypes.APPLICATION_JSON, MIMETypes.APPLICATION_JSON, MIMETypes.APPLICATION_JSON)
                .collect(Collectors.toList());
    }

    private final String mimeType;
    private static Service service;
    private static HttpClient client;
    private static int index;

    public TestRemoteTextualSpaceAccessControl(String mimeType) {
        this.mimeType = mimeType;
    }

    @Before
    public void setUp() throws Exception {
        tupleSpace = getTupleSpace();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        service.stop();
        service.awaitTermination(MAX_WAIT);
        client.close();
    }

    protected TextualSpace getTupleSpace() throws InterruptedException, ExecutionException, TimeoutException {
        return new RemoteTextualSpaceImpl(client, "localhost", 8080, "default-" + mimeType.replace("/", "-") + "-" + index++, mimeType, "user", "user");
    }

    @Test
    public void testClientsCannotBeCreatedWithWrongCredentials() throws InterruptedException, TimeoutException {
        try {
            new RemoteTextualSpaceImpl(client, "localhost", 8080, "default-" + mimeType.replace("/", "-") + "-" + (index - 1), mimeType, "wrongUsername-" + (index - 1), "wrongPassword-" + (index - 1));
        } catch (ExecutionException e) {
            assertTrue(e.getCause() instanceof HttpStatusError);
            final HttpStatusError cause = (HttpStatusError) e.getCause();
            assertEquals(401, cause.getStatusCode());
        }
    }

    @Test
    public void testUsersCannotImplicitlyCreateTupleSpacesWithOutPrimitive() throws InterruptedException, TimeoutException {
        try {
            tupleSpace.out("tuple").get(MAX_WAIT.toMillis(), TimeUnit.MILLISECONDS);
        } catch (ExecutionException e) {
            assertTrue(e.getCause() instanceof HttpStatusError);
            final HttpStatusError cause = (HttpStatusError) e.getCause();
            assertEquals(403, cause.getStatusCode());
        }
    }

    @Test
    public void testUsersCannotImplicitlyCreateTupleSpacesWithInPrimitive() throws InterruptedException, TimeoutException {
        try {
            tupleSpace.in("template").get(MAX_WAIT.toMillis(), TimeUnit.MILLISECONDS);
        } catch (ExecutionException e) {
            assertTrue(e.getCause() instanceof HttpStatusError);
            final HttpStatusError cause = (HttpStatusError) e.getCause();
            assertEquals(403, cause.getStatusCode());
        }
    }

    @Test
    public void testUsersCannotImplicitlyCreateTupleSpacesWithRdPrimitive() throws InterruptedException, TimeoutException {
        try {
            tupleSpace.rd("template").get(MAX_WAIT.toMillis(), TimeUnit.MILLISECONDS);
        } catch (ExecutionException e) {
            assertTrue(e.getCause() instanceof HttpStatusError);
            final HttpStatusError cause = (HttpStatusError) e.getCause();
            assertEquals(403, cause.getStatusCode());
        }
    }

    @Test
    public void testUsersCannotImplicitlyCreateTupleSpacesWithGetPrimitive() throws InterruptedException, TimeoutException {
        try {
            tupleSpace.get().get(MAX_WAIT.toMillis(), TimeUnit.MILLISECONDS);
        } catch (ExecutionException e) {
            assertTrue(e.getCause() instanceof HttpStatusError);
            final HttpStatusError cause = (HttpStatusError) e.getCause();
            assertEquals(403, cause.getStatusCode());
        }
    }

    @Test
    public void testUsersCannotImplicitlyCreateTupleSpacesWithGetSizePrimitive() throws InterruptedException, TimeoutException {
        try {
            tupleSpace.getSize().get(MAX_WAIT.toMillis(), TimeUnit.MILLISECONDS);
        } catch (ExecutionException e) {
            assertTrue(e.getCause() instanceof HttpStatusError);
            final HttpStatusError cause = (HttpStatusError) e.getCause();
            assertEquals(403, cause.getStatusCode());
        }
    }
}
